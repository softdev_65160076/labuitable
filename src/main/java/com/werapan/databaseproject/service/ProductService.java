/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
 
    public List<Product> getProducts(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll(" product_id asc");
    }

    public Product addNew(Product editProduct) {
        ProductDao productDao =new ProductDao();
        return productDao.save(editProduct);
    }

    public Product update(Product editProduct) {
        ProductDao productDao =new ProductDao();
        return productDao.update(editProduct);
    }

    public int delete(Product editProduct) {
        ProductDao productDao =new ProductDao();
        return productDao.delete(editProduct);
    }
  
}
