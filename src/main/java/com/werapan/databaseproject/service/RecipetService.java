/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.RecipetDao;
import com.werapan.databaseproject.dao.RecipetDetailDao;
import com.werapan.databaseproject.model.Recipet;
import com.werapan.databaseproject.model.RecipetDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecipetService {
    public  Recipet getById(int id){
        RecipetDao recipetDao = new RecipetDao();
        return recipetDao.get(id);
    }
    
    public List<Recipet> getRecipets(){
        RecipetDao recipetDao = new RecipetDao();
        return recipetDao.getAll(" recipet_id asc");
    }

    public Recipet addNew(Recipet editRecipet) {
        RecipetDao recipetDao =new RecipetDao();
        RecipetDetailDao recipetDetailDao =new RecipetDetailDao();
        Recipet recipet = recipetDao.save(editRecipet);
        for(RecipetDetail rd: editRecipet.getRecipetDeLists()){
            rd.setRecipetId(recipet.getId());
            recipetDetailDao.save(rd);
        }
        
        return recipet;
        
    }

    public Recipet update(Recipet editRecipet) {
        RecipetDao recipetDao =new RecipetDao();
        return recipetDao.update(editRecipet);
    }

    public int delete(Recipet editRecipet) {
        RecipetDao recipetDao =new RecipetDao();
        return recipetDao.delete(editRecipet);
    }
  
}
