/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.dao;

import com.werapan.databaseproject.helper.DatabaseHelper;
import com.werapan.databaseproject.model.Recipet;
import com.werapan.databaseproject.model.RecipetDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecipetDao implements Dao<Recipet> {

    @Override
    public Recipet get(int id) {
        Recipet recipet = null;
        String sql = "SELECT * FROM recipet WHERE recipet_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recipet = Recipet.fromRS(rs);
                RecipetDetailDao rdd = new RecipetDetailDao();
                ArrayList<RecipetDetail> recipetDetails = (ArrayList<RecipetDetail>) rdd.getAll(" recipet_id"+recipet.getId()," recipet_detail_id ");
                recipet.setRecipetDeLists(recipetDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recipet;
    }


    public List<Recipet> getAll() {
        ArrayList<Recipet> list = new ArrayList();
        String sql = "SELECT * FROM recipet";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Recipet recipet = Recipet.fromRS(rs);
                list.add(recipet);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Recipet> getAll(String where, String order) {
        ArrayList<Recipet> list = new ArrayList();
        String sql = "SELECT * FROM recipet where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Recipet recipet = Recipet.fromRS(rs);
                list.add(recipet);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Recipet> getAll(String order) {
        ArrayList<Recipet> list = new ArrayList();
        String sql = "SELECT * FROM recipet  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Recipet recipet = Recipet.fromRS(rs);
                list.add(recipet);
                

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Recipet save(Recipet obj) {

        String sql = "INSERT INTO recipet (total,cash, total_qty, user_id, customer_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getCustomerId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Recipet update(Recipet obj) {
        String sql = "UPDATE recipet"
                + " SET toal = ?, cash = ?, total_qty = ?, user_id = ?, customer_id = ?"
                + " WHERE recipet_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getUserId());
            stmt.setInt(5, obj.getCustomerId());
            stmt.setInt(6, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Recipet obj) {
        String sql = "DELETE FROM recipet WHERE recipet_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
