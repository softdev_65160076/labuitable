/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Recipet {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecipetDetail> recipetDetails = new ArrayList<RecipetDetail>();

    public Recipet(int id, Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }
    
    public Recipet( Date createdDate, float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }
    
    public Recipet( float total, float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.userId = userId;
        this.customerId = customerId;
    }
    
    public Recipet(float cash, int totalQty, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.userId = userId;
        this.customerId = customerId;
    }

    
    
    public Recipet() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getId();
    }

    public ArrayList<RecipetDetail> getRecipetDeLists() {
        return recipetDetails;
    }

    public void setRecipetDeLists(ArrayList recipetDetails) {
        this.recipetDetails = recipetDetails;
    }

    @Override
    public String toString() {
        return "Recipet{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + ", recipetDetails=" + recipetDetails + '}';
    }
    
    public void addRecipetDetail(RecipetDetail recipetDetail){
        recipetDetails.add(recipetDetail);
        calculateTotal();
    }
    
    public void delRecipetDetail(RecipetDetail recipetDetail){
        recipetDetails.remove(recipetDetail);
        calculateTotal();
    }
    
    private void calculateTotal(){
        int totalQty = 0;
        float total = 0.0f;
        for(RecipetDetail rd: recipetDetails){
            total +=  rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty =totalQty;
        this.total = total;
    }
    

    
    public static Recipet fromRS(ResultSet rs) {
        Recipet recipet = new Recipet();
        try {
            recipet.setId(rs.getInt("recipet_id"));
            recipet.setCreatedDate(rs.getTimestamp("created_date"));
            recipet.setTotal(rs.getFloat("total"));
            recipet.setCash(rs.getFloat("cash"));
            recipet.setTotal(rs.getInt("total_qty"));
            recipet.setUserId(rs.getInt("user_id"));
            recipet.setCustomerId(rs.getInt("customer_id"));
            
            CustomerDao customerDao = new CustomerDao();
            UserDao UserDao = new UserDao();
            Customer customer = customerDao.get(recipet.getCustomerId());
            User user = UserDao.get(recipet.getUserId());
            recipet.setCustomer(customer);
            recipet.setUser(user);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Recipet.class.getName()).log(Level.SEVERE, null, ex);
            
            return null;
        }
        return recipet;
    }
}
