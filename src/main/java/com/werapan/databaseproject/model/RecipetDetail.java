/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class RecipetDetail {
    private int id;
    private int   productId;
    private String   productName;
    private float   productPrice;
    private int   qty;
    private float   totalPrice;
    private int   recipetId;

    public RecipetDetail(int id, int productId, String productName, float productPrice, int qty, float totalPrice, int recipetId) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recipetId = recipetId;
    }
    
    public RecipetDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recipetId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recipetId = recipetId;
    }
    
    public RecipetDetail() {
        this.id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recipetId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecipetId() {
        return recipetId;
    }

    public void setRecipetId(int recipetId) {
        this.recipetId = recipetId;
    }

    @Override
    public String toString() {
        return "RecipetDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recipetId=" + recipetId + '}';
    }
    
    public static RecipetDetail fromRS(ResultSet rs) {
        RecipetDetail recipetDetail = new RecipetDetail();
        try {
            recipetDetail.setId(rs.getInt("recipet_detail_id"));
            recipetDetail.setProductId(rs.getInt("product_id"));
            recipetDetail.setProductName(rs.getString("product_name"));
            recipetDetail.setProductPrice(rs.getFloat("product_price"));
            recipetDetail.setQty(rs.getInt("qty"));
            recipetDetail.setTotalPrice(rs.getFloat("total_price"));
            recipetDetail.setRecipetId(rs.getInt("recipet_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recipetDetail;
    }
    
} 
