/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tsest_service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.dao.RecipetDao;
import com.werapan.databaseproject.dao.RecipetDetailDao;
import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.model.Recipet;
import com.werapan.databaseproject.model.RecipetDetail;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class TestRecipetDetailDao {
    public static void main(String[] args) {
        RecipetDetailDao rdd  = new RecipetDetailDao();
        for(RecipetDetail rd : rdd.getAll()){
            System.out.println(rd);
        }
        RecipetDao rd = new RecipetDao();
        ProductDao pd = new ProductDao();
        List<Product> product = pd.getAll();
        Product product0 = product.get(0);
        Recipet recipet = rd.get(1);
        RecipetDetail newRecipetDetail = new RecipetDetail(product0.getId(), product0.getName() 
                ,product0.getPrice() , 1, product0.getPrice()*1, recipet.getId());
        rdd.save(newRecipetDetail);
    }
}
